﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SuperheroRegistration.Models;

public partial class Superpower
{
    public int SuperpowerId { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    /*    public int CatgId { get; set; }
    */

    [ForeignKey("CatgID")]
    public virtual Category? Category { get; set; } = null!;

    public virtual ICollection<Category> Categories { get; set; } = new List<Category>();

    public virtual ICollection<Superhero> Superheroes { get; set; } = new List<Superhero>();
}
