﻿using Microsoft.EntityFrameworkCore;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using System.Collections.Generic;

namespace SuperheroRegistration.Repository
{
    public interface ISuperpowerRepository
    {
        IEnumerable<SuperpowerDTO> GetAll();
        SuperpowerDTO GetById(int SuperpowerID);
        bool SuperpowerExists(int SuperheroID);
    }
}

