﻿using System;
using System.Collections.Generic;

namespace SuperheroRegistration.Models;

public partial class SuperpowerCategory
{
    public int SuperpowerCategoryId { get; set; }

    public int SuperpowerId { get; set; }

    public virtual Category Catg { get; set; } = null!;

    public virtual Superpower Superpower { get; set; } = null!;
}
