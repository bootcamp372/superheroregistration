﻿using SuperheroRegistration.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SuperheroRegistration.Repository
{
    public class SuperpowerDTO
    {
        public int SuperpowerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public string? CatgName { get; set; }
        public string? CatgDescription { get; set; }

/*        [ForeignKey("CatgID")]
        public virtual Category? Category { get; set; } = null!;

        public virtual ICollection<CategoryDTO>? Category { get; set; }

        public virtual ICollection<CategoryDTO> Categories { get; set; }*/

        public virtual ICollection<SuperheroDTO> Superheroes { get; set; }

    }
}
