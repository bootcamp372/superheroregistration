
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Configuration;
using SuperheroRegistration.Models;
using SuperheroRegistration.Repository;

namespace SuperheroRegistration
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var MyAllowSpecificOrigins = "MyAllowSpecificOrigins";

            builder.Services.AddScoped<ISuperheroRepository, SuperheroRepository>();
            builder.Services.AddScoped<ISuperpowerRepository, SuperpowerRepository>();

            builder.Services.AddControllers()
            .AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddDbContext<SuperheroContext>(options => options.UseSqlServer(
            builder.Configuration.GetConnectionString(
            "Superhero")));
/*
            builder.Configuration.GetConnectionString(
            "Superhero");*/

            builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.WithOrigins("http://example.com",
                                "http://www.contoso.com",
                                "https://cors1.azurewebsites.net",
                                "https://cors3.azurewebsites.net",
                                "https://localhost:7255",
                                "http://localhost:4200",
                                "http://localhost/")
            .AllowAnyMethod()
            .AllowAnyHeader()
            .SetIsOriginAllowedToAllowWildcardSubdomains();
                    });
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseCors(MyAllowSpecificOrigins);

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}