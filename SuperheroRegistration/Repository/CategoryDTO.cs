﻿using SuperheroRegistration.Models;

namespace SuperheroRegistration.Repository
{
    public class CategoryDTO
    {
        public int CatgId { get; set; }

        public string Name { get; set; } = null!;

        public string? Description { get; set; }

/*        public virtual ICollection<SuperheroDTO>? Superheroes { get; set; }*/

    }
}
