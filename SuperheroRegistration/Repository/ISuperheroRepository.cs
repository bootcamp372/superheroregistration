﻿using SuperheroRegistration.Models;
using Microsoft.EntityFrameworkCore;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using System.Collections.Generic;

namespace SuperheroRegistration.Repository
{
    public interface ISuperheroRepository
    {
        IEnumerable<SuperheroDTO> GetAll();
        SuperheroDTO GetById(int SuperheroID);
        void Insert(Superhero superhero);
        void Update(Superhero superhero);
        void Delete(int SuperheroID);
        void Save();
        bool SuperheroExists(int SuperheroID);
        List<Superhero> GetSuperheroesByName(string search);
        List<Superhero> GetSuperheroesByPower(string search);
    }
}
