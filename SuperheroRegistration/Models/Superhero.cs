﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SuperheroRegistration.Models;

public partial class Superhero
{
    public int SuperheroId { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; } = null!;
    public string? Email { get; set; } = null!;

    public string Nickname { get; set; } = null!;

    public string PhoneNumber { get; set; } = null!;

    public DateTime? Dob { get; set; }
   
    public string? Alias { get; set; }

    public string? ImagePath { get; set; }

    [ForeignKey("WeaknessID")]
    public virtual Category? Weakness { get; set; }

    public virtual ICollection<Superpower> Superpowers { get; set; } = new List<Superpower>();

}
