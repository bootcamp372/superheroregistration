﻿using System;
using System.Collections.Generic;

namespace SuperheroRegistration.Models;

public partial class SuperheroSuperpower
{
    public int SuperheroId { get; set; }

    public int SuperpowerId { get; set; }

    /*    public int? NoteId { get; set; }

        public virtual Note? Note { get; set; }*/

    public virtual Superhero Superhero { get; set; } = null!;

    public virtual Superpower Superpower { get; set; } = null!;
}
