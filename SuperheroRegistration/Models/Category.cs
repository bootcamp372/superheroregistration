﻿using System;
using System.Collections.Generic;

namespace SuperheroRegistration.Models;

public partial class Category
{
    public int CatgId { get; set; }

    public string Name { get; set; } = null!;

    public string? Description { get; set; }

    public virtual ICollection<Superhero>? Superheroes { get; set; }

    public virtual ICollection<Superpower>? Superpowers { get; set; }

    public virtual ICollection<Superpower> SuperpowersNavigation { get; set; } = new List<Superpower>();
}
