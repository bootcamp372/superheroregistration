﻿using SuperheroRegistration.Models;
using Microsoft.EntityFrameworkCore;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http.HttpResults;

namespace SuperheroRegistration.Repository
{
    public class SuperheroRepository : ISuperheroRepository
    {
        static SuperheroContext _context = new SuperheroContext();

        public SuperheroRepository(SuperheroContext context)
        {
            _context = context;
        }
        public IEnumerable<SuperheroDTO> GetAll()
        {
             var superheroes = _context.Superheroes
                .Include(s => s.Superpowers)
                .ThenInclude(s => s.Category)
                .Include(s => s.Weakness).ToList().OrderBy(s => s.Name);

            var superheroDTOs = superheroes.Select(s => new SuperheroDTO
            {
                SuperheroId = s.SuperheroId,
                Name = s.Name,
                Nickname = s.Nickname,
                Description = s.Description,
                Email = s.Email,
                PhoneNumber = s.PhoneNumber,
                DOB = s.Dob,
                Alias = s.Alias,
                ImagePath = s.ImagePath != null ? s.ImagePath : "",
/*                Weakness = s.Weakness,*/
                WeaknessName = s.Weakness != null ? s.Weakness.Name : "N/A",
                WeaknessDescription = s.Weakness != null ? s.Weakness.Description : "N/A",
                Superpowers = s.Superpowers.Select(ss => new SuperpowerDTO
                {
                    SuperpowerId = ss.SuperpowerId,
                    Name = ss.Name,
                    Description = ss.Description
                    ,
                    /*                    Category = ss.Category,*/
                    CatgName = ss.Category != null ? ss.Category.Name : "N/A",
                    CatgDescription = ss.Category != null ? ss.Category.Description : "N/A",
                }).ToList()
            });

            return superheroDTOs;

/*            return _context.Superheroes.ToList();*/
/*            return _context.Superheroes.Include(s => s.Superpowers).ThenInclude(s => s.Category).Include(s => s.Weakness).ToList();
*/
        }

        public SuperheroDTO GetById(int SuperheroId)
        {
            // create list of superhero DTO
            // call superheroexists 
            var superheroes = _context.Superheroes
               .Include(s => s.Superpowers)
               .ThenInclude(s => s.Category)
               .Include(s => s.Weakness).ToList();

            var superheroesDTO = superheroes.Select(s => new SuperheroDTO
            {
                SuperheroId = s.SuperheroId,
                Name = s.Name,
                Nickname = s.Nickname,
                Description = s.Description,
                Email = s.Email,
                PhoneNumber = s.PhoneNumber,
                DOB = s.Dob,
                Alias = s.Alias,
                ImagePath = s.ImagePath != null ? s.ImagePath : "",
/*                Weakness = s.Weakness,*/
                WeaknessName = s.Weakness != null ? s.Weakness.Name : "N/A",
                WeaknessDescription = s.Weakness != null ? s.Weakness.Description : "N/A",
                Superpowers = s.Superpowers.Select(ss => new SuperpowerDTO
                {
                    SuperpowerId = ss.SuperpowerId,
                    Name = ss.Name,
                    Description = ss.Description
                    ,
                    /*                    Category = ss.Category,*/
                    CatgName = ss.Category != null ? ss.Category.Name : "N/A",
                    CatgDescription = ss.Category != null ? ss.Category.Description : "N/A",
                }).ToList()
            });

            if (SuperheroExists(SuperheroId) && superheroesDTO != null)
            {
                var superheroDTO = superheroesDTO.ToList().Find(s => s.SuperheroId == SuperheroId);
                return superheroDTO;
            }
            else {
                throw new ArgumentException("Superhero not found.");
            }

            /*            return _context.Superheroes.Include(s => s.Superpowers).ToList().Find(s => s.SuperheroId == SuperheroId);
            */
        }
        public void Insert(Superhero superhero)
        {
            var newHero = new SuperheroDTO();

            newHero.Name = superhero.Name;
            newHero.Nickname = superhero.Nickname;
            newHero.Description = superhero.Description;
            newHero.Email = superhero.Email;
            newHero.PhoneNumber = superhero.PhoneNumber;
            newHero.DOB = superhero.Dob;
            newHero.Alias = superhero.Alias;
            newHero.ImagePath = superhero.ImagePath;

            _context.Superheroes.Add(superhero);
            _context.Superheroes.Include(s => s.Superpowers);
        }
        public void Update(Superhero superhero)
        {
            var existingHero = GetById(superhero.SuperheroId);

            if (existingHero == null)
            {
                throw new ArgumentException("Superhero not found.");
            }

            existingHero.Name = superhero.Name;
            existingHero.Nickname = superhero.Nickname;
            existingHero.Description = superhero.Description;
            existingHero.Email = superhero.Email;
            existingHero.PhoneNumber = superhero.PhoneNumber;
            existingHero.DOB = superhero.Dob;
            existingHero.Alias = superhero.Alias;
            existingHero.ImagePath = superhero.ImagePath;

            _context.Entry(superhero).State = EntityState.Modified;
        }
        public void Delete(int SuperheroId)
        {
            Superhero superhero = _context.Superheroes.Find(SuperheroId);
            _context.Superheroes.Remove(superhero);
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        public bool SuperheroExists(int SuperheroId)
        {
            return (_context.Superheroes?.Any(s => s.SuperheroId == SuperheroId)).GetValueOrDefault();
        }

        public List<Superhero> GetSuperheroesByName(string search)
        {
            var matchingSuperheroes = _context.Superheroes
                .Where(s => s.Name.Contains(search) 
                    || s.Description.Contains(search)
                    || s.Nickname.Contains(search)
                    || s.Description.Contains(search)
                    || s.Alias.Contains(search)
                    )
                .OrderBy(p => p.Name)
                .ToList();

            return matchingSuperheroes;
        }

        public List<Superhero> GetSuperheroesByPower(string search)
        {
            var matchingPowers = _context.Superpowers
                .Where(p => p.Name.Contains(search) || p.Description.Contains(search))
                .OrderBy(p => p.Name)
                .Select(p => p.SuperpowerId)
                .ToList();

            var matchingSuperheroes = _context.Superheroes
                .ToList();

            return matchingSuperheroes;
        }

    }
}
