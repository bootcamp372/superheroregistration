﻿using System;
using System.Collections.Generic;

namespace SuperheroRegistration.Models;

public partial class Note
{
    public int NoteId { get; set; }

    public string? Filename { get; set; }

    public string? Filepath { get; set; }
}
