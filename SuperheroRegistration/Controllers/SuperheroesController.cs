﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol.Core.Types;
using SuperheroRegistration.Models;
using SuperheroRegistration.Repository;
using Microsoft.AspNetCore.Cors;


namespace SuperheroRegistration.Controllers
{
    [EnableCors("MyAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class SuperheroesController : ControllerBase
    {
        private ISuperheroRepository _superheroRepository;
        private readonly SuperheroContext _context;

        public SuperheroesController(SuperheroContext context, ISuperheroRepository superheroRepository)
        {
            _context = context;
            _superheroRepository = superheroRepository;
        }

        // GET: api/Superheroes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Superhero>>> GetSuperheroes()
        {
            if (_context.Superheroes == null)
            {
                return NotFound();
            }
            var superheroes = _superheroRepository.GetAll();
            if (superheroes == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(superheroes.ToList());
            }
        }

        // GET: api/Superheroes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SuperheroDTO>> GetSuperhero(int id)
        {
          if (_context.Superheroes == null)
          {
              return NotFound();
          }
            var superhero = _superheroRepository.GetById(id);

            if (superhero == null)
            {
                return NotFound();
            }

            return Ok(superhero);
        }

        // PUT: api/Superheroes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuperhero(int id, Superhero superhero)
        {
            if (id != superhero.SuperheroId)
            {
                return BadRequest();
            }

            _context.Entry(superhero).State = EntityState.Modified;

            try
            {
                _superheroRepository.Update(superhero);
                _superheroRepository.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_superheroRepository.SuperheroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Superheroes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Superhero>> PostSuperhero(Superhero superhero)
        {
          if (_context.Superheroes == null)
          {
              return Problem("Entity set 'SuperheroContext.Superheroes'  is null.");
          }
            _superheroRepository.Insert(superhero);
            _superheroRepository.Save();

            return CreatedAtAction("GetSuperhero", new { id = superhero.SuperheroId }, superhero);
        }

        // DELETE: api/Superheroes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuperhero(int id)
        {
            if (_context.Superheroes == null)
            {
                return NotFound();
            }
            var superhero = await _context.Superheroes.FindAsync(id);
            if (superhero == null)
            {
                return NotFound();
            }

            _superheroRepository.Delete(id);
            _superheroRepository.Save();

            return NoContent();
        }

        [HttpGet("search")]
        public ActionResult<IEnumerable<Superhero>> GetSuperheroesByName(string name)
        {
            if (_context.Superheroes == null)
            {
                return NotFound();
            }
            var superhero = _superheroRepository.GetSuperheroesByName(name);

            if (superhero == null)
            {
                return NotFound();
            }

            return Ok(superhero);
        }

        [HttpGet("search-power")]
        public ActionResult<IEnumerable<Superhero>> GetSuperheroesBySuperpower(string superpower)
        {
            var matchingSuperheroes = _superheroRepository.GetAll()
                .Where(s => s.Superpowers.Any(sp => sp.Name.Contains(superpower, StringComparison.OrdinalIgnoreCase)))
                .ToList();

            return Ok(matchingSuperheroes);
        }

    }
}
