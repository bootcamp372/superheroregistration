﻿using SuperheroRegistration.Models;
using SuperheroRegistration.Repository;
using System.ComponentModel.DataAnnotations.Schema;

namespace SuperheroRegistration.Repository
{
    public class SuperheroDTO
    {
        public int SuperheroId { get; set; }
        public string Name { get; set; }
        public string Nickname { get; set; }
        public string? Description { get; set; }
        public string? Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DOB { get; set; }
        public string? Alias { get; set; }
        public string? ImagePath { get; set; }
        public string? WeaknessName { get; set; }
        public string? WeaknessDescription { get; set; }

/*        [ForeignKey("WeaknessID")]
        public virtual Category? Weakness { get; set; }*/
        /*        public virtual ICollection<CategoryDTO>? Weakness { get; set; }*/
        public virtual ICollection<SuperpowerDTO>? Superpowers { get; set; }
    }
}
