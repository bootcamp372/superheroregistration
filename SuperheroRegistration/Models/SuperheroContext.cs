﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace SuperheroRegistration.Models;

public partial class SuperheroContext : DbContext
{
    public SuperheroContext()
    {
    }

    public SuperheroContext(DbContextOptions<SuperheroContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Note> Notes { get; set; }

    public virtual DbSet<Superhero> Superheroes { get; set; }

    public virtual DbSet<Superpower> Superpowers { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Superhero;Integrated Security=True;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>(entity =>
        {
            entity.HasKey(e => e.CatgId).HasName("PK__Category__20F311ADA1B21FA3");

            entity.ToTable("Category");

            entity.Property(e => e.CatgId).HasColumnName("CatgID");
            entity.Property(e => e.Name).HasMaxLength(50);
        });

        modelBuilder.Entity<Note>(entity =>
        {
            entity.HasKey(e => e.NoteId).HasName("PK__Notes__EACE355F674EA97F");

            entity.Property(e => e.Filename).HasMaxLength(255);
            entity.Property(e => e.Filepath).HasMaxLength(255);
        });

        modelBuilder.Entity<Superhero>(entity =>
        {
            entity.HasKey(e => e.SuperheroId).HasName("PK__Superher__05B02FF756877DE0");

            entity.ToTable("Superhero");

            entity.Property(e => e.SuperheroId).HasColumnName("SuperheroID");
            entity.Property(e => e.Alias).HasMaxLength(255);
            entity.Property(e => e.Dob)
                .HasColumnType("date")
                .HasColumnName("DOB");
            entity.Property(e => e.Name).HasMaxLength(50);
            entity.Property(e => e.Nickname).HasMaxLength(50);
            entity.Property(e => e.PhoneNumber).HasMaxLength(50);
            entity.Property(e => e.ImagePath);
            /*            entity.Property(e => e.WeaknessId).HasColumnName("WeaknessID");*/

            entity.HasOne(d => d.Weakness).WithMany(p => p.Superheroes)
/*                .HasForeignKey(d => d.WeaknessId)*/
                .HasConstraintName("FK__Superhero__Weakn__628FA481");

            entity.HasMany(d => d.Superpowers).WithMany(p => p.Superheroes)
                .UsingEntity<Dictionary<string, object>>(
                    "SuperheroSuperpower",
                    r => r.HasOne<Superpower>().WithMany()
                        .HasForeignKey("SuperpowerId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__Superhero__Super__7B5B524B"),
                    l => l.HasOne<Superhero>().WithMany()
                        .HasForeignKey("SuperheroId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__Superhero__Super__7A672E12"),
                    j =>
                    {
                        j.HasKey("SuperheroId", "SuperpowerId").HasName("PK__Superher__EBF531623B4CF0C2");
                        j.ToTable("SuperheroSuperpower");
                        j.IndexerProperty<int>("SuperheroId").HasColumnName("SuperheroID");
                        j.IndexerProperty<int>("SuperpowerId").HasColumnName("SuperpowerID");
                    });
        });

        modelBuilder.Entity<Superpower>(entity =>
        {
            entity.HasKey(e => e.SuperpowerId).HasName("PK__Superpow__E451E95FCF34AAB0");

            entity.ToTable("Superpower");

            entity.Property(e => e.SuperpowerId).HasColumnName("SuperpowerID");
/*            entity.Property(e => e.CatgId).HasColumnName("CatgID");*/
            entity.Property(e => e.Name).HasMaxLength(50);

            entity.HasOne(d => d.Category).WithMany(p => p.Superpowers)
/*                .HasForeignKey(d => d.CatgId)*/
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__Superpowe__CatgI__25869641");

            entity.HasMany(d => d.Categories).WithMany(p => p.SuperpowersNavigation)
                .UsingEntity<Dictionary<string, object>>(
                    "SuperpowerCategory",
                    r => r.HasOne<Category>().WithMany()
                        .HasForeignKey("CatgId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__Superpowe__CatgI__5DCAEF64"),
                    l => l.HasOne<Superpower>().WithMany()
                        .HasForeignKey("SuperpowerId")
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK__Superpowe__Super__5CD6CB2B"),
                    j =>
                    {
                        j.HasKey("SuperpowerId", "CatgId").HasName("PK__Superpow__265ED84510C2CA6E");
                        j.ToTable("SuperpowerCategory");
                        j.IndexerProperty<int>("SuperpowerId").HasColumnName("SuperpowerID");
                        j.IndexerProperty<int>("CatgId").HasColumnName("CatgID");
                    });
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
