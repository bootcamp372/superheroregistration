﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol.Core.Types;
using SuperheroRegistration.Models;
using SuperheroRegistration.Repository;
using Microsoft.AspNetCore.Cors;

namespace SuperheroRegistration.Controllers
{
    [EnableCors("MyAllowSpecificOrigins")]
    [Route("api/[controller]")]
    [ApiController]
    public class SuperpowersController : ControllerBase
    {
        private ISuperpowerRepository _superpowerRepository;
        private readonly SuperheroContext _context;

        public SuperpowersController(SuperheroContext context, ISuperpowerRepository superpowerRepository)
        {
            _context = context;
            _superpowerRepository = superpowerRepository;
        }

        // GET: api/Superpowers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Superpower>>> GetSuperpowers()
        {
            if (_context.Superpowers == null)
            {
                return NotFound();
            }
            var superpowers = _superpowerRepository.GetAll();
            if (superpowers == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(superpowers.ToList());
            }

        }

        // GET: api/Superpowers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Superpower>> GetSuperpower(int id)
        {
            if (_context.Superpowers == null)
            {
                return NotFound();
            }
            var superpower = _superpowerRepository.GetById(id);

            if (superpower == null)
            {
                return NotFound();
            }

            return Ok(superpower);
        }

/*        // PUT: api/Superpowers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSuperpower(int id, Superpower superpower)
        {
            if (id != superpower.SuperpowerId)
            {
                return BadRequest();
            }

            _context.Entry(superpower).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SuperpowerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }*/

/*        // POST: api/Superpowers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Superpower>> PostSuperpower(Superpower superpower)
        {
          if (_context.Superpowers == null)
          {
              return Problem("Entity set 'SuperheroContext.Superpowers'  is null.");
          }
            _context.Superpowers.Add(superpower);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSuperpower", new { id = superpower.SuperpowerId }, superpower);
        }

        // DELETE: api/Superpowers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSuperpower(int id)
        {
            if (_context.Superpowers == null)
            {
                return NotFound();
            }
            var superpower = await _context.Superpowers.FindAsync(id);
            if (superpower == null)
            {
                return NotFound();
            }

            _context.Superpowers.Remove(superpower);
            await _context.SaveChangesAsync();

            return NoContent();
        }*/

        private bool SuperpowerExists(int id)
        {
            return (_context.Superpowers?.Any(e => e.SuperpowerId == id)).GetValueOrDefault();
        }
    }
}
