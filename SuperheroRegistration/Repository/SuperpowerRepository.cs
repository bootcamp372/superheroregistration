﻿using SuperheroRegistration.Models;
using Microsoft.EntityFrameworkCore;

namespace SuperheroRegistration.Repository
{
    public class SuperpowerRepository : ISuperpowerRepository
    {
        static SuperheroContext _context = new SuperheroContext();

        public SuperpowerRepository(SuperheroContext context)
        {
            _context = context;
        }

        public IEnumerable<SuperpowerDTO> GetAll()
        {
            var superpowers = _context.Superpowers
               /*               .Include(sp => sp.Categories)*/
               .Include(sp => sp.Superheroes)
               .ToList().OrderBy(sp => sp.Name);

            var superpowerDTOs = superpowers.Select(sp => new SuperpowerDTO
            {
                SuperpowerId = sp.SuperpowerId,
                Name = sp.Name,
                Description = sp.Description,
                CatgName = sp.Category != null ? sp.Category.Name : "N/A",
                CatgDescription = sp.Category != null ? sp.Category.Description : "N/A",
                Superheroes = sp.Superheroes.Select(sh => new SuperheroDTO
                {
                    SuperheroId = sh.SuperheroId,
                    Name = sh.Name,
                    Nickname = sh.Nickname,
                    Description = sh.Description,
                    Email = sh.Email,
                    PhoneNumber = sh.PhoneNumber,
                    DOB = sh.Dob,
                    Alias = sh.Alias,
                    ImagePath = sh.ImagePath != null ? sh.ImagePath : "",
                    /*                Weakness = sh.Weakness,*/
                    WeaknessName = sh.Weakness != null ? sh.Weakness.Name : "N/A",
                    WeaknessDescription = sh.Weakness != null ? sh.Weakness.Description : "N/A"
                })
                .ToList()
            });
            return superpowerDTOs;
        }

        public SuperpowerDTO GetById(int SuperpowerId)
        {
            var superpowers = _context.Superpowers
               /*               .Include(sp => sp.Categories)*/
               .Include(sp => sp.Superheroes)
               .ToList().OrderBy(sp => sp.Name);

            var superpowersDTOs = superpowers.Select(sp => new SuperpowerDTO
            {
                SuperpowerId = sp.SuperpowerId,
                Name = sp.Name,
                Description = sp.Description,
                CatgName = sp.Category != null ? sp.Category.Name : "N/A",
                CatgDescription = sp.Category != null ? sp.Category.Description : "N/A",
                Superheroes = sp.Superheroes.Select(sh => new SuperheroDTO
                {
                    SuperheroId = sh.SuperheroId,
                    Name = sh.Name,
                    Nickname = sh.Nickname,
                    Description = sh.Description,
                    Email = sh.Email,
                    PhoneNumber = sh.PhoneNumber,
                    DOB = sh.Dob,
                    Alias = sh.Alias,
                    ImagePath = sh.ImagePath != null ? sh.ImagePath : "",
                    WeaknessName = sh.Weakness != null ? sh.Weakness.Name : "N/A",
                    WeaknessDescription = sh.Weakness != null ? sh.Weakness.Description : "N/A"
                })
                .ToList()
            }); ;

            if (SuperpowerExists(SuperpowerId) && superpowersDTOs != null)
            {
                var superpowerDTO = superpowersDTOs.ToList().Find(s => s.SuperpowerId == SuperpowerId);
                return superpowerDTO;
            }
            else
            {
                throw new ArgumentException("Superhero not found.");
            }
        }

        public bool SuperpowerExists(int SuperpowerId)
        {
            return (_context.Superpowers?.Any(s => s.SuperpowerId == SuperpowerId)).GetValueOrDefault();
        }
    }
}
